from tkinter import *
# from PIL import ImageTk, Image

master = Tk()
master.title('Simulador de Redes Ópticas')
master.geometry("1400x750")
# master.iconbitmap('.//Images//icon.ico')

txt = 'Default'

# create all of the main containers
top_frame = Frame(master, width=1400, height=50, pady=3, bd=3, relief=GROOVE)
btm_frame = Frame(master, width=1400, height=100, pady=3, bd=3, relief=GROOVE)
btm_frame2 = Frame(master, width=1400, height=50, pady=3)

center = Frame(master, width=1400, height=550, padx=3, pady=3)

ctr_left = Frame(center, width=100, height=550, bd=3, relief=GROOVE)
ctr_mid = Frame(center, width=1240, height=550, padx=3, pady=3, bd=3, relief=RIDGE)
ctr_right = Frame(center, width=60, height=550, padx=3, pady=3, bd=3, relief=GROOVE)

master.grid_rowconfigure(1, weight=1)
master.grid_columnconfigure(0, weight=1)

center.grid_rowconfigure(0, weight=1)
center.grid_columnconfigure(1, weight=1)

top_frame.grid(row=0, sticky="ew")
center.grid(row=1, sticky="nsew")
btm_frame.grid(row=3, sticky="ew")
btm_frame2.grid(row=4, sticky="ew")

ctr_left.grid(row=0, column=0, sticky="ns")
ctr_mid.grid(row=0, column=1, sticky="nsew")
ctr_right.grid(row=0, column=2, sticky="ns")

top_frame.pack_propagate(0)
center.pack_propagate(0)
btm_frame.pack_propagate(0)
btm_frame2.pack_propagate(0)

ctr_left.pack_propagate(0)
ctr_mid.pack_propagate(0)
ctr_right.pack_propagate(0)

canvas = Canvas(ctr_mid, width=1240, height=550, bg="white")
msg = Message(btm_frame, text='default', width=1380)


def config():
    global raio
    raio = 10


def gen_graph():
    path = 'topologia//europeia.txt'
    topology = open(path)
    graph = {}
    xi = 15
    yi = 15
    p = 60
    xo = 15
    yo = 15
    switch = 0
    for n in topology.readlines():
        line = n.split()
        try:
            if line[0] in graph:
                graph[line[0]].update({line[1]})
            else:
                graph[line[0]] = {line[1]:[xo, yo]}
                circle(raio, xo, yo, lbl=line[0])
                yo = yo + p
                if yo > 520:
                    yo = yi


        except ValueError:
            pass
    return graph


def motion(event):
    global msg
    txt = 'entrou {}'.format(event.x)
    msg.destroy()
    msg = Message(btm_frame, text=txt, width=1380)
    msg.pack()
    return


def circle(r, x, y, lbl='1', outline="red", fill="green", width=0.5):
    x0 = x + r
    y0 = y + r
    x1 = x - r
    y1 = y - r
    label = Label(canvas, text=lbl, fg='black', bg = 'green')
    label.bind('<Enter>', motion)
    canvas.create_window(x, y, window=label)
    canvas.create_oval(x0, y0, x1, y1, outline=outline, fill=fill, width=width)


def main():
    config()
    gen_graph()

    canvas.pack(expand=1)


if __name__ == "__main__":
    main()
    mainloop()
