import json
import Trafego
import Network
from numpy.random import poisson
from numpy import linspace
from numpy import log

bloqueio = 0

file_path = './/topologia//config.json'
json_data = json.load(open(file_path))
net_type = 'Eon'
eon_config = json_data[net_type]
t_max = json_data['iterations']


def loop(carga, tipo, topologia):
    h = log(carga)
    count = 0
    t_now = 0  # tempo atual
    t_aux = 0
    block = 0
    dif = poisson(8)
    traffic = Trafego.escolha(tipo)
    network = Network.Rede_Wdm(eon_config, './/topologia//' + topologia +'.txt')
    num_calls = 1
    # id_nodes = [0, 0]
    # duration = 0
    if tipo == 'static':
        traffic.random_traffic(network.max_node(), t_max*carga, h)
    # id_calls = {}
    while t_now < carga:
        if tipo != 'static':
            if (t_now - t_aux) > dif:
                count = count + 1
                t_aux = 0
                dif = poisson(8)
                traffic.random_traffic(network.max_node(), t_now, h)
        if traffic.begin_call(t_now):
            chs = []
            traffic.calls_process.sort(key=lambda c: c.arrival)
            for a in range(traffic.qnt_call(t_now)):
                traffic.calls_process.append(traffic.calls_request.pop(0))
                chs.append(traffic.calls_process[0])
            try:
                for ch in chs:
                    if tipo == 'dinamic':
                        id_nodes, bit_rate, duration = [[str(ch.origin), str(ch.destiny)], ch.bit_rate, ch.ending - ch.arrival]
                        num_calls = num_calls + 1
                        # string = '{} to {} for {} at {}'.format(ch.origin, ch.destiny, bit_rate, duration)
                        # id_calls[string] = id_nodes
                        path = network.route(id_nodes)
                        if path:
                            if net_type == 'Eon' and network.perform_call_eon(list(path), 0):
                                block = block + 1
                            elif net_type == 'Sdm' and network.perform_call_sdm(list(path), 0):
                                block = block + 1
                        else:
                            print (id_nodes)
                            print ('bloqueio')
                            block = block + 1
                    else:
                        id_nodes, bit_rate = [[str(ch.origin), str(ch.destiny)], ch.bit_rate]
                        num_calls = num_calls + 1
                        path = network.route(id_nodes)
                        if path:
                            if net_type == 'Eon' and network.perform_call_eon(list(path), 0):
                                block = block + 1
                            elif net_type == 'Sdm' and network.perform_call_sdm(list(path), 0):
                                block = block + 1
                        else:
                            print (id_nodes)
                            print ('bloqueio')
                            block = block + 1
            except NameError:
                print('except')
                break
        if tipo == 'dinamic':
            ending_call = traffic.end_call(t_now)
            for id_nodes in ending_call:
                id_nodes = [str(id_nodes[0]), str(id_nodes[1])]
                path = network.route(id_nodes)
                if net_type == 'Eon':
                    network.perform_call_eon(list(path), 1)
                elif net_type == 'Sdm':
                    network.perform_call_sdm(list(path), 1)
        # if len(traffic.calls_process) + len(traffic.calls_finalized) == iterations:
        #    pass
        t_now = t_now + 0.001
        block_p = block / num_calls
        # print("Time:{} Blocks:{} Calls:{} Ratio:{}".format(t_now,block,num_calls,block_p))
    block_p = block/num_calls
    return [num_calls, block, t_now]


def main():
    carga = [20, 50, 80]
    tipo = ['static', 'incremental', 'dinamic']
    resultado = []
    for c in carga:
        for t in tipo:
            print ('Tipo: %s. Carga: %d' % (t, c))
            [nc, bl, t] = loop(c, t, 'europeia')
            resultado.append([nc, bl, t])
            print([nc, bl, t])
            print ('')
    print ('')
    print ('')
    print (resultado)

    


if __name__ == '__main__':
    main()
