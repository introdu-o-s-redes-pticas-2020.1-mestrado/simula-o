clear;
clc;
dados = [
  % Tipo: static. Carga: 50
  98, 33, 100.00000000011343;
  % Tipo: incremental. Carga: 50
  74907, 74842, 100.00000000011343;
  % Tipo: dinamic. Carga: 50
  76779, 59010, 100.00000000011343;
  % Tipo: static. Carga: 150
  97, 32, 100.00000000011343;
  % Tipo: incremental. Carga: 150
  76905, 76840, 100.00000000011343;
  % Tipo: dinamic. Carga: 150
  78265, 57442, 100.00000000011343;
  % Tipo: static. Carga: 250
  97, 32, 100.00000000011343;
  % Tipo: incremental. Carga: 250
  76544, 76479, 100.00000000011343;
  % Tipo: dinamic. Carga: 250
  76393, 54221, 100.00000000011343
  ];
nomes = {'Bloqueadas', 'Aceitas'};
titulos = {
    'Tr�fego Est�tico com Carga de 50 Elang',
    'Tr�fego Incremental com Carga de 50 Elang',
    'Tr�fego Din�mico com Carga de 50 Elang',
    'Tr�fego Est�tico com Carga de 150 Elang',
    'Tr�fego Incremental com Carga de 150 Elang',
    'Tr�fego Din�mico com Carga de 150 Elang',
    'Tr�fego Est�tico com Carga de 250 Elang',
    'Tr�fego Incremental com Carga de 250 Elang',
    'Tr�fego Din�mico com Carga de 250 Elang'
    };
for a = 1:length(dados)
    dados(a,3) = dados(a,1) - dados(a,2);
end
dados = [dados(:,2), dados(:,3)];
figure ()
for a = 1:length(dados)
    subplot(3,3,a);
    pie(dados(a,:));
    title(titulos(a));
    legend(nomes);
end