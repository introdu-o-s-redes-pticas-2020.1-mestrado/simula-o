import matplotlib.pyplot as plt
import numpy as np

def func (pct, dados):
  total = int(pct/100.0*np.sum(dados))
  return '{:.f}%\n({:d})'.format(pcr, total)

label = ['Aceitas', 'Bloqueadas']
dados = [50, 50]
fig, ax = plt.subplots(figsize(6,6), subplot_kw='dict'(aspect='equal'))
wedges, texts, autotexts = ax.pie(dados, autopct=lambda pct: func(pct, dados),
textprops=dict(color='w'))
ax.legend(wedges, recipe, title='Chamadas',
loc='center left',
bbox_to_anchor=(1,0,0.5,1))
plt.setp(autotexts, size=8, weight='bold')
ax.set_title('Chamadas pata tipo tatal com carga tal')
plt.show()