# !/usr/bin/python
# -*- coding: utf-8 -*-

class Link:
    def __init__(self, d_amp, n_slot, n_core=1):
        self.amp_Location = d_amp
        self.slots = n_slot
        self.max_slots = n_slot
        self.cores = n_core
        self.max_cores = n_core

    def use_slot(self):
        if self.slots > 0:
            self.slots = self.slots - 1
            return 0
        return 1

    def free_slot(self):
        if self.slots < self.max_slots:
            self.slots = self.slots + 1
            return 0
        return 1
    
    def use_core(self):
        if self.cores > 0:
            self.cores = self.cores - 1
            return 0
        return 1
    
    def free_core(self):
        if self.cores < self.max_cores:
            self.cores = self.cores + 1
            return 0
        return 1

    @staticmethod
    def noise_Ase(gain):
        """ gain é o ganho do amplificador em db
            booster = nois_ASE - ganho do sss
            in_line = nois_ASE - ganho do segmento de fibra
            pre = noise_ASE - ganho do segmento de fibra"""
        # Dados retirados do simulador de Cavalcante
        # Ganho do SSS = 5
        # Ganho do segmento de fibra = 0.22 * distancia
        h = 6.62606957e-34
        freq = 193.4e12
        bRef = 12.5e9
        numPol = 2
        return 0.5*numPol*h*freq*bRef*(gain-1)*(10**5)
