# !/usr/bin/python
# -*- coding: utf-8 -*-

from .Router import Router
from .Graph import Graph
from .Node import Node
from .Link import Link


class Rede_Wdm:
    def __init__(self, config, path):
        topology = open(path)
        self.node = {}
        self.link = {}
        graph = {}
        self.graphDis = Graph([])
        linhas = topology.readlines()
        linhas.pop(0)
        for n in linhas:
            line = n.split()
            try:
                if line[0] in graph:
                    self.graphDis.add_edge(str(line[0]), str(line[1]), 1)
                    graph[line[0]].update({line[1]: int(line[2])})
                    istr = line[0] + ' to ' + line[1]
                    self.link[istr] = Link(config["d_amp"], config["n_slot"])
                else:
                    self.graphDis.add_edge(str(line[0]), str(line[1]), 1)
                    graph[line[0]] = {line[1]: int(line[2])}
                    istr = line[0] + ' to ' + line[1]
                    self.link[istr] = Link(config["d_amp"], config["n_slot"])
                    self.node[line[0]] = Node(line[0])
            except ValueError:
                pass
        self.router = Router(graph)

    def route(self, id_nodes):
        # self.router.call(id_nodes)
        # self.router.djikstra()
        # return self.router.getRoute()
        try:
            algo = self.graphDis.dijkstra(id_nodes[0], id_nodes[1])
            return algo
        except KeyError:
            print (id_nodes)
            print ('id incorreto')
            return None
        except AssertionError:
            print (id_nodes)
            print ('assertion')
            exit ()

    def perform_call_eon(self, path, free=0):
        path = list(path)
        for id_2 in path[1:]:
            id_1 = path.pop(0)
            istr = id_1 + ' to ' + id_2
            if free == 0:
                temp = self.link[istr].use_slot()
                return temp
            elif free == 1:
                temp = self.link[istr].free_slot()
                return temp

    def perform_call_sdm(self, path, free=0):
        path = list(path)
        for id_2 in path[1:]:
            id_1 = path.pop(0)
            istr = id_1 + ' to ' + id_2
            if free == 0:
                if self.link[istr].use_slot():
                    temp = self.link[istr].use_core()
                    return temp
            elif free == 1:
                if self.link[istr].free_slot():
                    temp = self.link[istr].free_core()
                    return temp
    
    def max_node(self):
        return len(self.node)
