#!/usr/bin/python
#-*- coding: utf-8 -*-


class Router:
    """Implementação da classe responsável pelo roteamento"""
    def __init__(self, graph):
        #self.max_Iteration = 1000
        self.graph = graph
        self.route = [[]]
        self.start = '1'
        self.finish = '4'

    def call(self, id_nodes):
        self.start = str(id_nodes[0])
        self.finish = str(id_nodes[1])

    def djikstra(self, ):
        print(self.finish)
        path = [self.start]
        border = {self.start: 0, 'error': [1e25, 'error']}
        path_cost = 0
        end = path[-1]
        lag = []
        local_graph = self.graph
        begin = 1
        while end != self.finish or begin == 1:
            begin = 0
            border.pop(end)
            for node in local_graph[end].keys():
                value = local_graph[end][node] + path_cost
                if node in border.keys():
                    value = min(border[node][0], value)
                if node not in path:
                    border.update({node: [value, end]})

            local_graph.pop(end)
            end = 'error'
            lag.append('error')
            for x in border.keys():
                if border[x][0] < border[end][0]:
                    end = x
                    lag[-1] = border[end][1]

            path.append(end)
            path_cost = path_cost + border[end][0]


        i = 1
        while 0 < len(lag):
            print(path)
            print(lag)
            if lag[-1] != path[-i - 1]:
                path.pop(len(path) - i - 1)
            else:
                while path[-i - 1] in lag: lag.remove(path[-i - 1])
                while len(lag) >= len(path): lag.pop(len(lag) - 1)
                i = i + 1
        print(path)
        self.route = path

    def getRoute(self, ):
        return self.route


def main():
    #router = Router([1,4])
    return


if __name__ == '__main__':
    main()

