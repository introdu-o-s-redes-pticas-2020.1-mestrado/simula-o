# !/usr/bin/python
# -*- coding: utf-8 -*-

class Traffic:
    def __init__(self):
        self.calls_request = []
        self.calls_process = []
        self.calls_finalized = []
    
    def begin_call(self, t_now):
        # ''' Verifica se alguma chamada chegou no tempo t_now '''
        if len(self.calls_request) > 0:
            if round(self.calls_request[0].arrival, 3) == round(t_now, 3):
                return True
            else:
                return False
        else:
            return False

    def qnt_call(self, t_now):
        # ''' Verifica quantas chamadas chegam no tempo t_now'''
        c = 0
        for ch in self.calls_request:
            if round(ch.arrival, 3) == round(t_now, 3):
                c = c + 1
        return c
    
    def end_call(self, t_now):
        # '''Verifica se alguma chamada foi finalizada'''
        aux = []
        if len(self.calls_process) > 0:
            c = 0
            for a in range(len(self.calls_process)):
                ch = self.calls_process[a]
                if round(ch.ending, 3) == round(t_now, 3):
                    c = c + 1
            self.calls_process.sort(key=lambda b: b.ending)
            for a in range(c):
                aux.append([self.calls_process[0].origin, self.calls_process[0].destiny])
                self.calls_finalized.append(self.calls_process.pop(0))
            self.calls_process.sort(key=lambda b: b.arrival)
        return aux
