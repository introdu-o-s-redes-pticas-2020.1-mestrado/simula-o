from .Dinamic import Dinamic
from .Incremental import Incremental
from .Static import Static


def escolha(tipo):
    if 'static' == tipo:
        traffic = Static()
    elif 'incremental' == tipo:
        traffic = Incremental()
    else:
        traffic = Dinamic()
    return traffic
