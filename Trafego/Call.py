#!/usr/bin/python
#-*- coding: utf-8 -*-


class Call:
    def __init__(self, origin=0, destiny=0, bit_rate=0, permanent=False, arrival=0, ending=0, finalized=False):
        self.origin = origin
        self.destiny = destiny
        self.bit_rate = bit_rate
        self.permanent = permanent
        self.arrival = arrival
        self.ending = ending
        self.finalized = finalized
        self.has = 0
        self.get_hash()
    
    def get_arrival(self):
        return self.arrival
    
    def __str__(self):
        return str(self.origin) + ' ' + str(self.destiny)
    
    def get_hash(self):
        self.has = 0
        if self.origin:
            self.has = self.has + self.origin
        if self.destiny:
            self.has = self.has + (2*self.destiny)
        if self.bit_rate:
            self.has = self.has + (3*self.bit_rate)
        if self.arrival:
            self.has = self.has + (4*self.arrival)
        if self.ending:
            self.has = self.has + (5*self.ending)
        self.has = int(self.has)
