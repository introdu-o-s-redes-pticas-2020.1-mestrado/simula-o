#!/usr/bin/python
#-*- coding: utf-8 -*-

from random import randint
from numpy.random import exponential
from numpy.random import uniform
from Trafego.Traffic import Traffic
from Trafego.Call import Call


class Dinamic(Traffic):
    def call_generation(self, node_max, t_now, h):
        # nome_max é o valor o maior do da rede
        # t_now é o tempo atual
        origin = randint(1, node_max)
        while True:
            destiny = randint(1, node_max)
            if origin != destiny:
                arrival = round(t_now + exponential(h), 3)
                ending = round(arrival + exponential(1/h), 3)
                self.calls_request.append(Call(origin, destiny, randint(1, node_max), False, arrival, ending))
                return

    def random_traffic(self, node_max, t_now, h):
        self.call_generation(node_max, t_now, h)
        self.calls_request.sort(key=lambda a: a.arrival)