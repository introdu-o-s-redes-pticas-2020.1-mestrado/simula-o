# !/usr/bin/python
# -*- coding: utf-8 -*-

from random import randint
from numpy.random import exponential
from .Traffic import Traffic
from .Call import Call


class Static(Traffic):
    def call_generation(self, node_max, t_now, h):
        # nome_max é o valor o maior do da rede
        # t_now é o tempo atual
        origin = randint(1, node_max)
        while True:
            destiny = randint(1, node_max)
            if origin != destiny:
                arrival = round(t_now + exponential(h), 3)
                self.calls_request.append(Call(origin, destiny, randint(1, node_max), True, arrival))
                return

    def random_traffic(self, node_max, qnt, h):
        for a in range(qnt):
            self.call_generation(node_max, a, h)
        self.calls_request.sort(key=lambda b: b.arrival)
